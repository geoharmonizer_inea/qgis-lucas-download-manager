# ST_LUCAS QGIS Plugin

QGIS plugin for accessing harmonized space-time aggregated LUCAS (Land
Use and Coverage Area frame Survey) data provided by the ST_LUCAS
system.

<img src=help/source/images/st_lucas_architecture.png width=650/><br>
<i>ST_LUCAS system architecture.</i>

## Funding

This work is co-financed under Grant Agreement Connecting Europe
Facility (CEF) Telecom project 2018-EU-IA-0095 by the European Union.
