Analyze
-------

The second tab is used to analyze the downloaded harmonized LUCAS data. It consists of two parts - LUCAS land cover classes aggregation and nomenclature translation. 

.. image:: images/analyze_tab.png

Class aggregation
*****************

The first part is used for LUCAS land cover classes aggregation. The user has to load the ``json`` file with the defined aggregation using the ``...`` tool button. There are two ``json`` files. One for aggregating LUCAS classes to the first level of LUCAS nomenclature and the other to the second level of LUCAS nomenclature. These two files are stored in ``lc_aggregation`` subdirectory of the plugin directory. The user can also define their own aggregation.

A sample input JSON file is `here <https://gitlab.com/geoharmonizer_inea/st_lucas/st_lucas-qgis-plugin/-/raw/main/lc_aggregation/aggregation_lc1_h_to_level1.json>`_.

When the ``json`` file with the defined aggregation is loaded, groups of aggregation are displayed.

.. image:: images/load_json.png

User can click to one of these groups to show, which classes are aggregated to the specific group.

.. image:: images/aggregated_groups.png

When clicked on ``Apply aggregation`` button, new column is added to the attribute table.

.. image:: images/att_table.png

Nomenclature translation
************************

The second part is used for nomenclature translation. The user has to load the ``csv`` file with defined translation using the ``...`` tool button and choose the target column of the translation.

A sample input CSV file is `here <https://gitlab.com/geoharmonizer_inea/st_lucas/st_lucas-python-package/-/raw/main/st_lucas/analyze/nomenclature_translation/LUCAS_unique_lc1_lu1_combinations_ALL.csv>`_.

.. image:: images/translation.png

When clicked on ``Apply translation`` button, new column is added to the attribute table.

.. image:: images/att_table2.png

.. note::

   The nomenclature translation function is still in its initial version and is intended for further development.
